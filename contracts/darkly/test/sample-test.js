const { expect } = require("chai");

describe("Darkly", function() {
  it("Should return the new customer once it's changed", async function() {
    const Darkly = await ethers.getContractFactory("Darkly");
    const darkly = await Darkly.deploy('0x0000000000000000000000000000000000000000');

    await darkly.deployed();
    expect(await darkly.getCustomer()).to.equal("");

    await darkly.setCustomer("git@github.com:Leibniz137/darkly.git");
    expect(await darkly.getCustomer()).to.equal("git@github.com:Leibniz137/darkly.git");
  });
});
